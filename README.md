# GROUP9 Competence dag met Lego Mindstorms

## Voorbereiding

Om ervoor te zorgen dat je goed voorbereid op de Competence dag bent, vragen we je het volgende te doen:

- zorg ervoor dat er een `SSH client` op je laptop staat, hiermee kun je verbinding maken met de Lego Mindstorms brick op de Competence dag zelf
- aanname is dat je IntelliJ met JDK 11 geinstalleerd hebt
- selecteer het bestand `build.gradle` en daarna de optie `Link Gradle Project`
- zorg ervoor dat in sectie `preferences > build > build tools > gradle` de `gradle vm` ook Java 11 aangeeft
- voer de task `build > jar` uit bij het tabblad `Gradle`, standaard staat deze aan de rechterkant
- mocht je handmatig `./gradlew jar` willen uitvoeren en krijg je de melding `Could not find or load main class org.gradle.wrapper.GradleWrapperMain`, voer dan het commando `gradle wrapper` uit om dit te fixen
- als de uitkomst `BUILD SUCCESSFUL` is, ben je klaar voor de Competence dag
