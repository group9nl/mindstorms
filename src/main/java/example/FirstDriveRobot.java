package example;

import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;
import ev3dev.sensors.Battery;
import ev3dev.sensors.ev3.EV3ColorSensor;
import ev3dev.sensors.ev3.EV3IRSensor;
import ev3dev.sensors.ev3.EV3TouchSensor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FirstDriveRobot {

	private static class FigureTraverse {
		public FigureTraverse(boolean startLeft90, boolean endPreferLeft) {
			this.startLeft90 = startLeft90;
			this.endPreferLeft = endPreferLeft;
		}

		private boolean startLeft90;
		private boolean endPreferLeft;
	}

	private static final FigureTraverse[] figureTraverses = new FigureTraverse[] {
			new FigureTraverse(true, true), //first is the starting figure
			new FigureTraverse(false, false),
			new FigureTraverse(false, false),
			new FigureTraverse(false, true),
			new FigureTraverse(true, true),
			new FigureTraverse(false, false),
			new FigureTraverse(false, false)
	};

	public static final EV3LargeRegulatedMotor MOTOR_LEFT = new EV3LargeRegulatedMotor(MotorPort.B);
	public static final EV3LargeRegulatedMotor MOTOR_RIGHT = new EV3LargeRegulatedMotor(MotorPort.C);

	private static EV3ColorSensor colorSensor = new EV3ColorSensor(SensorPort.S3);
	private static EV3TouchSensor touchSensor = new EV3TouchSensor(SensorPort.S2);

	public static final int SLOW_SPEED = 200;
	public static final double DEGREES_FACTOR = 19.5 * 3600.0 / 3620.0;
	private static float[] irsample;
	public static boolean motorBoolean = false;

	public static Logger LOGGER = LoggerFactory.getLogger(FirstDriveRobot.class);

	private static final int[] DISTANCE_BETWEEN_FIGURES = {
			0,
			65,
			65,
			72,
			60,
			50,
			65
	};

//	private static final boolean[] PREFER_LEFT = {
//			true,
//			false,
//			false,
//			false,
//			false,
//			false,
//			false,
//			true,
//			true
//	};
//
//	private static final int activeFigure = 0;


	public static void main(String[] args) {
		stopMotorsAtShutdown(MOTOR_LEFT, MOTOR_RIGHT);

		MOTOR_LEFT.setSpeed(SLOW_SPEED);
		MOTOR_RIGHT.setSpeed(SLOW_SPEED);

		grandFinale();

//		colorSensor.setFloodlight(Color.NONE);
//		LOGGER.info(colorSensor.getFloodlight() + "ba");
//		colorSensor.setFloodlight(Color.RED);
//		LOGGER.info(colorSensor.getFloodlight() + "ba");
//		circle();
//		house();
		//squareWithDiagonal();
		//driveBehindDistanceSensor();
		//driveWithColorSensor();

		MOTOR_LEFT.stop();
		MOTOR_RIGHT.stop();
	}

	private static void grandFinale() {
		int currentTraverse = 0;

		while (true) {
			driveAcrossLine(figureTraverses[currentTraverse].endPreferLeft);
			if  (isBlockHit()) {
				currentTraverse++;

				if (figureTraverses[currentTraverse].startLeft90) {
					turnLeft(90);
				} else {
					turnRight(90);
				}
			}
		}
	}

	private static void driveAcrossLine(boolean preferLeft) {
		brake();
		if (seesColor()) {
			forward(1);
		} else {
			int counter = 0;
			while (counter < 10) {
				if (seesColor()) {
					brake();
					return;
				}
				seek(preferLeft, 1);
				counter++;
			}

			if (!seesColor()) {
				if (preferLeft) {
					turnRight(counter * 5);
				} else {
					turnLeft(counter * 5);
				}
			}
			brake();

			counter = 0;
			while (counter < 10) {
				if (seesColor()) {
					brake();
					return;
				}
				seek(!preferLeft, 1);
				counter++;
			}
		}
	}

	private static void seek(boolean preferLeft, int counter) {
		if (preferLeft) {
			seekColorLeft(counter);
		} else {
			seekColorRight(counter);
		}
	}

	private static void driveWithColorSensor() {
		LOGGER.info("Going to drive with color sensor");
		int counter = 0;
		while (true) {
			brake();
			if (seesColor()) {
				counter = 0;
				LOGGER.info("COLOR SEEN DRIVING FOWARD");
				forward(1);
			} else {
				seekColor(counter);
				counter += 1;
			}
		}
	}

	private static void seekColor(int counter) {
		LOGGER.info("COLOR GONE, SENSING...");
		if (!seekColorLeft(counter)) {
			seekColorRight(counter);
		}
	}

	private static boolean seekColorLeft(int counter){
		int degrees = counter * 5;
		turnLeft(degrees);
		brake();
		if (!seesColor()) {
			turnRight(degrees);
			brake();
			return false;
		}
		return true;
	}

	private static boolean seekColorRight(int counter) {
		int degrees = counter * 5;
		turnRight(degrees);
		brake();
		if (!seesColor()) {
			turnLeft(degrees);
			brake();
			return false;
		}
		return true;
	}

	private static void brake() {
		if (motorBoolean) {
			MOTOR_LEFT.stop();
			MOTOR_RIGHT.stop();
			motorBoolean = false;
		} else {
			MOTOR_RIGHT.stop();
			MOTOR_LEFT.stop();
			motorBoolean = true;
		}
	}

	private static void squareWithDiagonal() {
		forward(97);
		turnLeft(90);

		forward(97);
		turnLeft(90);

		forward(97);
		turnLeft(90);

		forward(97);
		turnLeft(135);

		forward(160);
	}

	private static void house() {
		forward(102);
		turnRight(90);

		forward(98);
		turnRight(90);

		forward(99);
		turnRight(0.0);

		forward(61);
		turnRight(0.0);

		forward(100);
	}

	private static void circle() {
		for (int i = 0; i < 19; i++) {
			forward(20.46);
			turnLeft(360.0 / 20.0);
		}
	}

	private static void turnLeft(double degrees) {
		MOTOR_LEFT.backward();
		MOTOR_RIGHT.forward();
		var milliseconds = degrees * DEGREES_FACTOR + 10;
		Delay.msDelay((long) milliseconds);
	}

	private static void turnRight(double degrees) {
		MOTOR_LEFT.forward();
		MOTOR_RIGHT.backward();
		var milliseconds = degrees * DEGREES_FACTOR;
		Delay.msDelay((long) milliseconds);
	}

	private static void forward(double centimeters) {
		MOTOR_LEFT.forward();
		MOTOR_RIGHT.forward();
//		Delay.msDelay((long) (centimeters * 205));
		Delay.msDelay((long) (centimeters * 102));
	}




	private static boolean seesObstacleLeft() {
		return irsample[0] < 0.1;
	}

	private static boolean seesObstacleRight() {
		return irsample[1] < 0.1;
	}

	private static boolean seesColor() {
		SampleProvider sp = colorSensor.getRedMode();
		int sampleSize = sp.sampleSize();
		float[] sample = new float[sampleSize];

		int value = 0;

		LOGGER.info("Detecting color");

		// Takes some samples and prints them
		sp.fetchSample(sample, 0);
		value = (int) sample[0];

		LOGGER.info("Value = " + value);
		return (value >= 10);
	}


	private static void stopMotorsAtShutdown(EV3LargeRegulatedMotor motorLeft, EV3LargeRegulatedMotor motorRight) {
		//To Stop the motor in case of pkill java for example
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			System.out.println("Emergency Stop");
			motorLeft.stop();
			motorRight.stop();
		}));
	}

	private static boolean isBlockHit() {
		float[] sample = new float[1];
		touchSensor.getTouchMode().fetchSample(sample, 0);
		LOGGER.info("Touch sensor: {}", sample[0]);
		return sample[0] > 0.0f;
	}


}
