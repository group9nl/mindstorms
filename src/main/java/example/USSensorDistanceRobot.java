package example;

import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;
import ev3dev.sensors.Battery;
import ev3dev.sensors.ev3.EV3UltrasonicSensor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class USSensorDistanceRobot {

    public static Logger LOGGER = LoggerFactory.getLogger(USSensorDistanceRobot.class);

    private static final EV3UltrasonicSensor us1 = new EV3UltrasonicSensor(SensorPort.S4);

    public static void main(String[] args) {

        final SampleProvider sp = us1.getDistanceMode();
        int distanceValue;

        LOGGER.info("Starting motor on A");
        final EV3LargeRegulatedMotor mA = new EV3LargeRegulatedMotor(MotorPort.B);
        LOGGER.info("Starting motor on B");
        final EV3LargeRegulatedMotor mB = new EV3LargeRegulatedMotor(MotorPort.C);

        stopMotorsAtShutdown(mA, mB);


        mA.setSpeed(60);
        mB.setSpeed(60);
        mA.forward();
        mB.forward();

        final int iteration_threshold = 50;
        for (int i = 0; i <= iteration_threshold; i++) {

            float[] sample = new float[sp.sampleSize()];
            sp.fetchSample(sample, 0);
            distanceValue = (int) sample[0];

            LOGGER.info("Iteration: {}, Distance: {}", i, distanceValue);
            if (distanceValue <= 30) {
                mA.brake();
                mB.brake();
                LOGGER.info("breaking");
            }
            Delay.msDelay(100);
        }

        LOGGER.info("Battery voltage: {}", Battery.getInstance().getVoltage());

    }

    private static void stopMotorsAtShutdown(EV3LargeRegulatedMotor mA, EV3LargeRegulatedMotor mB) {
        //To Stop the motor in case of pkill java for example
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Emergency Stop");
            mA.stop();
            mB.stop();
        }));
    }

}
