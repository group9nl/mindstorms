# some examples to run
Following classes can be put in the MANIFEST.MF file of the application.

# speaking
example.robotics.ev3.actuator.TTSDemo

# detection
example.robotics.ev3.sensor.ColorSensorDetectBlueBall
example.robotics.ev3.sensor.TouchSensorExample
example.robotics.ev3.sensor.TurnWithGyroSensorExample

# moving
example.ForwardBackwardRobot
example.USSensorDistanceRobot